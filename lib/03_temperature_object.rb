class Temperature
  def initialize(hash = {})
    @temp_hash = hash
  end

  def in_fahrenheit
    if @temp_hash.key?(:f)
      return @temp_hash[:f]
    elsif @temp_hash.key?(:c)
      return Temperature.ctof(@temp_hash[:c])
    end

    nil
  end

  def in_celsius
    if @temp_hash.key?(:c)
      return @temp_hash[:c]
    elsif @temp_hash.key?(:f)
      return Temperature.ftoc(@temp_hash[:f])
    end

    nil
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.ftoc(temp)
    5.0 / 9 * (temp - 32)
  end

  def self.ctof(temp)
    temp * 9.0 / 5 + 32
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temp_hash = { c: temp }
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temp_hash = { f: temp }
  end
end
