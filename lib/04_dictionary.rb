class Dictionary
  attr_reader :entries, :keywords

  def initialize
    @entries = {}
    @keywords = @entries.keys
  end

  def add(entry)
    if entry.class == String
      @entries[entry] = nil
    else
      entry.each { |k, v| @entries[k] = v }
    end

    @keywords = @entries.keys.sort
  end

  def include?(keyword)
    return true if @keywords.include?(keyword)
    false
  end

  def find(prefix)
    @entries.select { |k, _v| k.include?(prefix) }
  end

  def printable
    printing = ""
    @keywords.each do |k|
      printing += "[#{k}] \"#{@entries[k]}\""
      printing += "\n" if k != @keywords.last
    end

    printing
  end
end
