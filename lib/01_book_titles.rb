class Book
  attr_reader :title

  def title=(title)
    restricted = ["and", "but", "a", "the", "in", "of", "an", "to"]
    words = title.split
    words[0].capitalize!
    words.each_index do |idx|
      words[idx].capitalize! unless restricted.include?(words[idx])
    end

    @title = words.join(" ")
  end
end
