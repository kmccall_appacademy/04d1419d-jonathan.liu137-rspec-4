class Friend
  def greeting(who = nil)
    return "Hello, #{who}!" if who
    "Hello!"
  end
end
