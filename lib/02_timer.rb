class Timer
  attr_reader :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hr = @seconds / 3600
    min = (@seconds - hr * 3600) / 60
    sec = @seconds - hr * 3600 - min * 60

    "#{padded(hr)}:#{padded(min)}:#{padded(sec)}"
  end

  def seconds=(sec)
    @seconds = sec
  end

  def padded(num)
    num_str = num.to_s
    until num_str.length == 2
      num_str = "0" + num_str
    end

    num_str
  end
end
